#include <Wire.h>

#include <seesaw_motor.h>
#include <Adafruit_NeoTrellis.h>
#include <Adafruit_miniTFTWing.h>
#include <seesaw_servo.h>
#include <seesaw_neopixel.h>
#include <Adafruit_TFTShield18.h>
#include <Adafruit_seesaw.h>
#include <Adafruit_Crickit.h>


Adafruit_seesaw ss;

void setup() {
  Serial.begin(9600);
  
  if (!ss.begin(0x36)) {
    Serial.println("ERROR! sensor not found");
    while(1);
  } else {
    Serial.print("sensor started! version: ");
    Serial.println(ss.getVersion(), HEX);
  }
}

void loop() {
  uint16_t capread = ss.touchRead(0);
  float tempC = ss.getTemp();
  float VWC_seesaw;
  int val = capread;
  float VWC_vegetronix;
  int sensor1DN = analogRead(0);
  float sensorVoltage = sensor1DN*(3.0/ 614.0);

 // val = map (val, 200.0, 2000.0, 0.00, 100.0);

 
    if(sensorVoltage <= 1.1) {
    VWC_vegetronix = 10*sensorVoltage-1;
  } else if(sensorVoltage > 1.1 && sensorVoltage <= 1.3) {
    VWC_vegetronix = 25*sensorVoltage-17.5;
  } else if(sensorVoltage > 1.3 && sensorVoltage <= 1.82) {
    VWC_vegetronix = 48.08*sensorVoltage-47.5;
  } else if(sensorVoltage > 1.82 && sensorVoltage <= 2.2) {
    VWC_vegetronix = 26.32*sensorVoltage-7.89;
  }
    else if(sensorVoltage > 2.2) {
    VWC_vegetronix = 62.5*sensorVoltage-87.5;
  }
  
 // Serial.print("VWC_seesaw: "); Serial.println(val);
  Serial.print("VWC_seesawRaw: "); Serial.println(capread);
  Serial.print("VWC_temperature: "); Serial.println(tempC);
  Serial.print("VWC_vegetronix: "); Serial.println(VWC_vegetronix);
  //long sleepInterval = 1000 * 6
  //delay(1000000);
  delay(1200000);
  
}
