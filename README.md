# evi-soilSensor

## How to run

from a terminal window, run `nohup python3 soil_moisture_logger.py &`

The above allows the program to run in the background and it will continue to run after you logout of the RPi. It will also write all the output to a local file `nohup.out`.

## How to stop logging
Assuming the program was run using the above `nohup...` command, you will need to do the following to stop the program:
1. In a terminal window, run `ps -ef | grep 'soil_moisture_logger'`
2. Take note of the process ID returned by the previous step, in the following example output the process ID is 1135:
```shell
pi        1135   905  0 14:11 pts/0    00:00:00 python3 soil_moisture_logger.py
pi        1173   905  0 14:13 pts/0    00:00:00 grep --color=auto soil_moisture_logger

```

3. In the same terminal window, run `kill -9 1234` replacing 1234 with the process ID from the previous command.


## To pull the output file down to your local machine, do something like this:
`scp pi@env-rpi1.agron.iastate.edu:Documents/enviratron-soil-sensor/evi-soilsensor/nohup.out ./log.txt`

And then you can update line #47 of of parser.py to match `log.txt` or whatever file name you used and then run `python parser.py > log.csv`

Then Excel should open that csv file.