import json
from collections import OrderedDict

def main(filepath):

    with open(filepath, "r") as read_handle:


        results_dict = OrderedDict()

        for line in read_handle:

            if not line.startswith("2019"):
                continue

            line_list = line.rstrip().split(" ")
            #print(line_list[0], line_list[1], line_list[-1])

            datetime_str = " ".join(line_list[:2])

            if datetime_str not in results_dict:
                results_dict[datetime_str] = {}


            if "vegetronix" in line:
                results_dict[datetime_str]["vegetronix"] = float(line_list[-1])
            elif "temperature" in line:
                results_dict[datetime_str]["temperature"] = float(line_list[-1])
            elif "seesawRaw" in line:
                results_dict[datetime_str]["seesaw raw"] = float(line_list[-1])


    #print(json.dumps(results_dict, indent=3))

    for k in results_dict:

	d = results_dict[k]

	try:
        	date, time = k.split(" ")
	except ValueError:
		date, time = k.split(",")[:2]

        print(",".join([date, time, str(d.get("vegetronix")), str(d.get("temperature")), str(d.get("seesaw raw"))]))






if __name__ == "__main__":
    fp = "./log.txt"
    main(fp)

